/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */
const getDeck = function() {
  //const card = {
  //  val: 10,
  //  displayVal: 'Jack'
  //  suit: 'hearts',
  //};
  let newDeck = [];
  suits = ['hearts', 'spades', 'clubs', 'diamonds'];
  for(let s in suits) {
    for (let i = 2;i<=10;i++) {
      let c = { 'val':i,'displayVal':String(i),'suit':suits[s] };
      newDeck.push(c);
    }
    newDeck.push( { 'val':10,'displayVal':'Jack','suit':suits[s] } );
    newDeck.push( { 'val':10,'displayVal':'Queen','suit':suits[s] } );
    newDeck.push( { 'val':10,'displayVal':'King','suit':suits[s] } );
    newDeck.push( { 'val':11,'displayVal':'Ace','suit':suits[s] } );
  }
  return newDeck;
}



// CHECKS
/*
const deck = getDeck();
console.log(`Deck length equals 52? ${deck.length === 52}`);

const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard && 
    randomCard.displayVal && 
    typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);
*/