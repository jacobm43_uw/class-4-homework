const foodIsCooked = function foodIsCooked(kind, internalTemp, doneness) {
    if (kind === 'chicken' && internalTemp > 165){
      return true;
    }
    else if (kind === 'beef'){
      if (doneness === 'rare' && internalTemp > 125){
        return true;
      }
      if (doneness === 'medium' && internalTemp > 135){
        return true;
      }
      if (doneness === 'well' && internalTemp > 155){
        return true;
      }
    }
  
    return false;
  }

  //Tests:
console.log(foodIsCooked('chicken',170,'na'))
console.log(foodIsCooked('chicken',140,'na'))
console.log(foodIsCooked('beef',130,'rare'))
console.log(foodIsCooked('beef',140,'medium'))
console.log(foodIsCooked('beef',160,'well'))
console.log(foodIsCooked('beef',100,'rare'))
console.log(foodIsCooked('beef',100,'medium'))
console.log(foodIsCooked('beef',100,'well'))
console.log(foodIsCooked('beef',200,'na'))