const blackjackDeck = getDeck();

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
const CardPlayer = function(name) {
  // CREATE FUNCTION HERE
  this.name = name;
  this.hand = [];
  this.drawCard = function (){
      this.hand.push( blackjackDeck[Math.floor(Math.random() * 52)] );
    }
};

// CREATE TWO NEW CardPlayers
let dealer = new CardPlayer('Dealer'); // TODO
let player = new CardPlayer('Player'); // TODO

/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} - Object containing Total points and whether hand isSoft
 */
const calcPoints = function(hand) {
  // CREATE FUNCTION HERE
  tempHand = hand.slice(0);
  tempHand.sort(function(a, b){return a.val - b.val});
  let total = 0;
  let isSoft = false;
  for (let i = 0; i < tempHand.length; i++) {
    if (tempHand[i].displayVal === 'Ace'){
      if ( total + 11 > 21 )
        total +=  1;
      else {
        total +=  11;
        isSoft = true;
      }
    } else {
      total += tempHand[i].val;
    }
  }
  return { 'total':total ,'isSoft':isSoft };
}

/**
 * Determines whether the dealer should draw another card
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 * If the dealer's hand is 16 points or less, the dealer must draw another card
 * If the dealer's hand is exactly 17 points, and the dealer has an Ace valued at 11, the dealer must draw another card
 * Otherwise if the dealer's hand is 17 points or more, the dealer will end her turn
 */
const dealerShouldDraw = function(dealerHand) {
  points = calcPoints(dealerHand);
  if (points.total <= 16) {
    return true;
  }
  else if (points.total === 17 && points.isSoft) {
    return true;
  }
  else {
    return false;
  }
}

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} States the player's score, the dealer's score, and who wins
 */
const determineWinner = function(playerScore, dealerScore) {
  let winner;
  if (playerScore > dealerScore ){
    winner = 'player wins'
  } else if ( playerScore < dealerScore ) {
    winner = 'dealer wins'
  }
  else {
    winner = 'game is a tie'
  }
  return `Player\'s Score is ${playerScore}, dealer\'s score is ${dealerScore}, ${winner}`
}

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = function(count, dealerCard) {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = function(player) {
  let displayHand = player.hand.map(function(card) { return card.displayVal});
  console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
  display(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
}

/**
 * Adds string to DOM
 * @param {String} toDisplay
 */
const display = function(toDisplay) {
  const h1El = document.createElement('h1');
  const h1TextNode =document.createTextNode(toDisplay);
  h1El.appendChild(h1TextNode);
  const body = document.getElementsByTagName('body')[0];
  body.appendChild(h1El);
}

/**
 * Runs Blackjack Game
 */
const startGame = function() {
  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  let playerScore = calcPoints(player.hand).total;
  if (playerScore === 21) {
    return 'You got 21 - you win!';
  }
  dealer.drawCard();
  let dealerScore = calcPoints(dealer.hand).total;
  if (dealerScore === 21) {
    return 'Dealer got 21 - you lose!';
  }

  showHand(player);
  while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
    player.drawCard();
    playerScore = calcPoints(player.hand).total;
    showHand(player);
  }
  if (playerScore > 21) {
    return 'You went over 21 - you lose!';
  }
  display(`Player stands at ${playerScore}`);
  console.log(`Player stands at ${playerScore}`);

  // IMPLEMENT DEALER LOGIC BELOW
  while (dealerScore < 21 && dealerShouldDraw(dealer.hand) ) {
    dealer.drawCard();
    dealerScore = calcPoints(dealer.hand).total;
    showHand(dealer);
  }
  if (dealerScore > 21) {
    return 'Dealer went over 21 - you win!';
  }
  display(`Dealer stands at ${dealerScore}`);
  console.log(`Dealer stands at ${dealerScore}`);

  return determineWinner(playerScore, dealerScore);
}
let result = startGame()
console.log(result);
display(result);